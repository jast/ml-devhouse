#!/usr/bin/python

import numpy as np
import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

k = 3

Xtr, Ytr = mnist.train.images, mnist.train.labels
Xte, Yte = mnist.test.images, mnist.test.labels

xtr = tf.placeholder("float", [None, 784])
ytr = tf.placeholder("float", [None, 10])
xte = tf.placeholder("float", [784])

###@doc
### <section>
###   <p>Let's try to improve. Can we use smaller images?
set_scaled = tf.image.resize_images(
    tf.reshape(xtr, [-1, 28, 28, 1]), [16, 16],
    tf.image.ResizeMethod.BICUBIC)
set_reshape = tf.reshape(set_scaled, [-1, 256])

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    Xtr = sess.run(set_reshape, feed_dict={xtr: Xtr})
    sess.run(init)
    Xte = sess.run(set_reshape, feed_dict={xtr: Xte})

xtr = tf.placeholder("float", [None, 256])
xte = tf.placeholder("float", [256])
### </section>
###@/doc

distance = tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(xtr, xte)), reduction_indices=1))
_, indices = tf.nn.top_k(tf.negative(distance), k=k, sorted=False)

nn = tf.stack([tf.argmax(ytr[indices[i]], 0) for i in range(k)])
y, _, count = tf.unique_with_counts(nn)
pred = tf.slice(y, begin=[tf.argmax(count, 0)], size=[1])[0]

accuracy = 0
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)

    for i in range(len(Yte)):
        nn_index = sess.run(pred, feed_dict={xtr: Xtr, ytr: Ytr, xte: Xte[i, :]})
        actual = np.argmax(Yte[i])
        if nn_index == actual:
            accuracy += 1
        print("Test %i prediction %i actual %i, cur acc %.02f%%" % (i, nn_index, actual, 100.*accuracy/(i+1)))

###@doc
### <section>
###   <table><tr><th>#Iterations</th><th>Accuracy</th></tr>
###     <tr><td>1000</td><td>95.50%</td></tr>
###     <tr><td>2500</td><td>95.44%</td></tr>
###     <tr><td>5000</td><td>95.54%</td></tr>
###     <tr><td>10000</td><td>96.83%</td></tr>
###   </table>
###   <p>Runtime of 14 minutes, error rate: 3.17%<br>
###   4.6% more errors but much faster
### </section>
