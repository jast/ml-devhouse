#!/usr/bin/python

###@doc
### <section>
###   <p>Load libraries and data

import numpy as np
import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

### </section>
### <section>
###   <p>Variables

k = 3

Xtr, Ytr = mnist.train.images, mnist.train.labels
Xte, Yte = mnist.test.images, mnist.test.labels

xtr = tf.placeholder("float", [None, 784])
ytr = tf.placeholder("float", [None, 10])
xte = tf.placeholder("float", [784])

### </section>
### <section>
###   <p>Computation in plain (math) language
###   <ul>
###     <li>Training instance: \(x\), sample: \(x'\)
###     <li>Distance: $$\sqrt{\sum_{i} \left( x_{i} - x'_{i} \right)^2}$$
###   </ul>
### </section>
### <section>
###   <p>Computation network

distance = tf.sqrt(tf.reduce_sum(
    tf.square(tf.subtract(xtr, xte)),
    reduction_indices=1))
_, indices = tf.nn.top_k(tf.negative(distance), k=k,
    sorted=False)

nn = tf.stack(
    [tf.argmax(ytr[indices[i]], 0) for i in range(k)])
y, _, count = tf.unique_with_counts(nn)
pred = tf.slice(y, begin=[tf.argmax(count, 0)], size=[1])[0]

### </section>
### <section>
###   <p>Prepare TensorFlow

accuracy = 0
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)

### </section>
### <section>
###   <p>Do it!

    for i in range(len(Yte)):
        nn_index = sess.run(pred,
            feed_dict={xtr: Xtr, ytr: Ytr, xte: Xte[i, :]})
        actual = np.argmax(Yte[i])
        if nn_index == actual:
            accuracy += 1
        print("Test %i prediction %i actual %i, cur acc %.02f%%" % (i, nn_index, actual, 100.*accuracy/(i+1)))

### </section>
### <section>
###   <p>Results for first attempt
###   <table><tr><th>#Iterations</th><th>Accuracy</th></tr>
###     <tr><td>1000</td><td>96.20%</td></tr>
###     <tr><td>2500</td><td>95.56%</td></tr>
###     <tr><td>5000</td><td>95.56%</td></tr>
###     <tr><td>10000</td><td>96.97%</td></tr>
###   </table>
###   <p>Runtime of 34 minutes, error rate: 3.03%<br>
###   Pretty good!
### </section>
